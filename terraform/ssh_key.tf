resource "serverspace_ssh" "ecdsa" {
  name = "ecdsa"
  public_key = "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBLTgwNV80R2TKPRpNGmaHB7ae7ODT9+V2xBnhKJom/0dLlqzR/Kt+gvB73n37CpUaL7oNk24sb6kHjDxX7ZCIb8= alex@alex-MS-7C52"
}

variable "pvt_key" {
  type = string
  default = "/home/alex/.ssh/id_ecdsa"
}