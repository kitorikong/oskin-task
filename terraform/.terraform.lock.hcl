# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/itglobalcom/serverspace" {
  version     = "0.2.3"
  constraints = "0.2.3"
  hashes = [
    "h1:m56v93fu8kLVF+i1b47s8acomZpYcYveLXxqEV2/kBA=",
    "zh:0a09c989a13dbfc6b40d9232d7a88909e51c0be0dfe030fa85cd567b6d842d62",
    "zh:0e9cffc8e1621d912d9a539e1162e2b372d31850661e2b852a1a0133282646a4",
    "zh:2065ac578c5d6d2cb2674cb9e75f924022598ccc24e5199cb395632ee20c01f8",
    "zh:225ac5ed9f74631bdf4edb3381512dfc0ced609df65f6850da95fda1f0122886",
    "zh:28ccaba89d2de6194f86fe73d0b9b38b99ca235ee98c79266d5ca86a211be468",
    "zh:4ed354b9a1e3d6578be91e4efbec9ad877b0236aaab384fca2d031be18f2096f",
    "zh:6cc6eda733043771fbe9039967053c23a5660306cdea37a7b154527f3d647fda",
    "zh:733f7f87254e562246e707644bd3897a81568a0bcf1c8659e3d2b004bb36fc32",
    "zh:90bea12f9aa0d2e77948bac9fdba082dffa5db7b7b05e6342964369a6315fcbf",
    "zh:9c91fd8964137f95db7075b23faad34d345f58b2d75adcab0e02bb3f3e9bebda",
    "zh:aca23653aead466016be5aee4c5ce94f8168b3ef203abb05d4803c09ce8fe938",
    "zh:b7b967bb1de312191c4b0b63753c2488ea65ca0ba61ed7a432af43760442e244",
    "zh:bf0a9bdf50b1a46d76f143596eba7fbb540b878b41ec001169f9acc39d1c9ea6",
    "zh:d93b2e90143e768140955dcfb6fcdcae7f5567fcf25f18443785d7f08a87c59b",
  ]
}
