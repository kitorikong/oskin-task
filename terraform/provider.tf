terraform {
  required_providers {
    serverspace = {
      source = "itglobalcom/serverspace"
      version = "0.2.3"
    }
  }
}

variable "s2_token" {
  type = string
  default = var.api_key
}

provider "serverspace" {
  key = var.s2_token
}