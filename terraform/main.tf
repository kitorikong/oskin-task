resource "serverspace_server" "Oskin-serverspace" {
  image = "Ubuntu-20.04-X64"
  name = "Oskin"
  location = "am2"
  cpu = 2
  ram = 2048

  boot_volume_size = 40*1024

  volume {
    name = "bar"
    size = 20*1024
  }

  nic {
    network = ""
    network_type = "PublicShared"
    bandwidth = 100
  }

  ssh_keys = [
    resource.serverspace_ssh.ecdsa.id,
  ]

  connection {
    host = self.public_ip_addresses[0]
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  # provisioner "remote-exec" { # Comment this block if you want to configure Docker via Ansible
  #   inline = [
  #     "export PATH=$PATH:/usr/bin",
  #     "sudo apt install apt-transport-https ca-certificates curl software-properties-common",
  #     "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -",
  #     "sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable'",
  #     "sudo apt update",
  #     "sudo apt install docker-ce",
  #     "sudo systemctl enable docker",
  #     "sudo curl -L 'https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)' -o /usr/local/bin/docker-compose",
  #     "sudo chmod +x /usr/local/bin/docker-compose",
  #     "exit 0"
  #   ]
  # }

}
